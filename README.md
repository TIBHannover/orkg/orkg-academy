# ORKG Academy

This repository contains the content of the Open Research Knowledge Graph (ORKG) Academy.

The rendered output can be viewed here: https://tibhannover.gitlab.io/orkg/orkg-academy/orkg-academy/main/index.html.

## Building the documentation locally

It is build using [Antora](https://antora.org/) and written in [Asciidoc](https://asciidoc.org/).
To install Antora, check their [quickstart](https://docs.antora.org/antora/latest/install-and-run-quickstart/) or [installation instruction](https://docs.antora.org/antora/latest/install/install-antora/).

To build the documentation, run `npx antora antora-playbook.yml`.
To fetch the lastest updates of all repositories, add the `--fetch` parameter: `npx antora antora-playbook.yml`.
