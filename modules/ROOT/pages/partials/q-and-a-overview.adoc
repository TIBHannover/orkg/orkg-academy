_Q&A sections_ are there to answer questions you might have.

****
[format="dsv", separator=";" ,width="100%",cols="1"]
[frame="none",grid="none"]
|======
xref:q-and-as/a-shared-resource-cannot-be-edited-directly.adoc[]
xref:q-and-as/change-password.adoc[]
xref:q-and-as/forgot-password.adoc[]
xref:q-and-as/how-can-i-delete-my-account.adoc[]
xref:q-and-as/how-can-i-set-up-an-observatory-in-the-orkg.adoc[]
xref:q-and-as/how-can-my-observatory-get-started.adoc[]
xref:q-and-as/how-do-i-add-new-terms-to-the-orkg.adoc[]
xref:q-and-as/how-do-i-assign-comparisons-and-papers-to-an-observatory.adoc[]
xref:q-and-as/how-to-insert-a-latex-and-asciimath-formula.adoc[]
xref:q-and-as/is-it-possible-to-link-an-observatory-to-more-than-one-research-field.adoc[]
xref:q-and-as/what-are-the-benefits-of-running-an-observatory.adoc[]
xref:q-and-as/what-effort-is-required-for-running-an-observatory.adoc[]
xref:q-and-as/what-topic-should-i-choose-for-an-observatory.adoc[]
xref:q-and-as/who-can-be-added-as-a-member-to-an-observatory.adoc[]
xref:q-and-as/who-can-run-an-observatory.adoc[]
xref:q-and-as/why-can-certain-resources-used-in-contribution-descriptions-not-be-edited.adoc[]
xref:q-and-as/why-should-i-contribute-to-the-orkg.adoc[]
xref:q-and-as/why-were-my-papers-unlisted.adoc[]
|======
****