= Creating a comparison from papers in the ORKG

This tutorial shows you how you can create an ORKG Comparison from papers that have already been added to the ORKG beforehand. To accomplish this, you have two options:

== Option 1 - Using the Contribution Editor
- Step 1: xref:no-category/sign-in.adoc[Sign in]

- Step 2: Click on _Add new_ and then _Comparison_

image::comp_addnew.PNG[]

- Step 3: This leads you to an overview page for adding comparisons. Choose option 2 "Contribution editor"

image::comp_chooseeditor.PNG[]

- Step 4: Now the contribution editor has opened. To add your first contribution click on _Add contribution_ on the top right.

image::comp_contreditor.PNG[]

- Step 5: A pop-up window will appear. Here, you can now search for the relevant papers by entering the paper title or DOI. Then, click on _Add contributions_ 

image::comp_popup.PNG[]

Repeat this step until all relevant contributions are added to your comparison. 

If you additionally want to add a paper to the comparison that is not yet in the ORKG, you can follow the rest of xref:tutorials/comparison-tutorial-i.adoc[this tutorial].

- Step 6: Once you have added at least 2 contributions, click on View comparison. Now you can see a preview of the comparison that you can tweak to your liking. Please also see here, for information on how to xref:courses/comparison-course.adoc#_comparison_organization[organize the comparison].

- Step 7: If you are satisfied with the comparison, you either publish your comparison. You can also save it as draft and come back to work on it later. You can find details on how to do that xref:courses/comparison-course.adoc#_publishing_or_saving_a_comparison[here].

## Option 2: Using the Contribution Selection

This option is very useful if you are browsing content in the ORKG and want to create a comparison from interesting contributions you came across.

If you are viewing a paper, you always have the option to add all contributions of that paper to a comparison by selecting the check-box labelled _Add to comparison_.

image::paper_addtocomp.PNG[]

This opens a red pop-up on the bottom right of your browser window. It shows you all contributions that you have currently selected. If you want to add more, you can do so by continue browsing ORKG papers. Don't worry, the contributions you have already selected, will stay selected. You can also remove selected contributions by clicking on the x. 

image::comp_contributionselect.PNG[]

If you are happy with your selection, click on _Start comparison_.

Now you are ready to move on to the next step and tweak your comparison to your liking by clicking xref:courses/comparison-course.adoc#_comparison_organization[here].