= I forgot my password. What do I do now?

If you forgot your password, please write an email to info@orkg.org. There you will receive help and your password will be reset for you, such that you can choose a new one.

== Related Questions
* xref:no-category/sign-in.adoc[]
* xref:q-and-as/change-password.adoc[]