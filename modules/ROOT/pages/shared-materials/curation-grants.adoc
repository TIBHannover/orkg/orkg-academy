= Curation Grants

The TIB Leibniz Information Centre for Science and Technology regularly invites applications for ORKG Curation Grants, to which researchers (advanced PhD students welcome) from various fields can apply. Successful applicants will make regular contributions to the ORKG in their research field, for which they will be personally compensated with 400 EUR per month. Grants initially run for six months, with the possibility of extension, and require you to invest approximately one day per week contributing to the ORKG. We expect grantees to add key research questions and corresponding research contributions in their research field to the ORKG. Grantees will be part of a joint effort to contribute to one of the biggest challenges in research―better organizing the contents of scholarly publications in their fields.

== 2021 Curation Grants
The 1st ORKG curation grant program ran from July to December 2021. In total, over 150 Comparisons and 12 Reviews were created. The link below provides an overview of our grantees' works. There, you can also find their presentations about their best Comparison.

See the xref:shared-materials/grant-results-2021.adoc[contributions and results of the 2021 Curation Grants].

== 2022 Curation Grants
The 2nd ORKG curation grant program took place in 2022. 12 grantees created nearly 200 comparisons and 8 reviews. They covered a variety from domains and topics. The following page lists their work and contains their final presentation about their experiences working with the ORKG.

See the xref:shared-materials/grant-results-2022.adoc[contributions and results of the 2022 Curation Grants].

== 2023 Curation Grants
The 3rd ORKG curation grant program took place in 2023. 9 researchers contributed roughtly 150 comparisons and 10 reviews. 

See the xref:shared-materials/grant-results-2023.adoc[contributions and results of the 2023 Curation Grants].

== 2024 Curation Grants
The 4th ORKG curation grant prgram is currently in preparation. You can still register by https://docs.google.com/forms/d/e/1FAIpQLSe2_swk3FMi1bGFmpVNDz-S6nZeMAWNyVh0FMQzVO4LAhUuUw/viewform[filling out the application form, window=read-later] and join the ORKG grantees!