= Semantic Modelling in the ORKG

When adding content to the ORKG, you are relatively free on how to model your data. While this flexibility can be helpful it can also be overwhelming. In this article, some style guidelines modeling best practices are listed. These guidelines are not rules that must be strictly followed. If your use case requires a different approach, that is fine. However, it is important to be consistent in your approach.

== Modelling Dos and Don'ts for xref:concepts/properties.adoc[Properties]

*Reuse existing properties whenever possible.*
[options="header"]
|======
|*Do* |*Don't*
|Reuse the property "URL" as it is already there |Create a new property to add a URL
|======

*Create properties that can be reused*
[options="header"]
|======
|*Do* |*Don't*
|Create a property "temperature" for describing methods |Create a property "temperature in degrees Celsius"
|======

*Add a general description for a property*
[options="header"]
|======
|*Do* |*Don't*
|Property "URL" has a description: "A Uniform Resource Locator (URL) to refer to a web resource" |Property URL has a description: "The URL where the source code of our project is located" (not generalizable, too specific) or no description at all
|======

*Create singular properties*
[options="header"]
|======
|*Do* |*Don't*
|Property with label "method" |Property with label "methods"
|======

*Short property labels, without prefixes such as "has" or "is"*
[options="header"]
|======
|*Do* |*Don't*
|Property with label "method" |Property with label "has method"
|======

*Capitalization: Do not start with a capital if not needed and only use captials where you would in English sentences.*
[options="header"]
|======
|*Do* |*Don't*
|Property with label "created by" |Property with label "Created by" or "created By"
|======

== Modelling Dos and Don'ts for xref:concepts/resources.adoc[Resources and Literals]

*Use short resource labels*
[options="header"]
|======
|Do| Don't
|Resource label "Classification" |Resource label "Classification is the process in which ideas and objects are recognized" (you should use a literal instead)
|======

*Decompose resource labels into atomic parts*
[options="header"]
|======
|Do| Don't
|Resource "Temperature"-[Value]->"20.5" and "Temperature"-[Unit]->"Degrees Celsius" (use two statements to describe the temperature) |Resource "Temperature 20.5 Degree Celsius"
|======

*Capitalization: Do not start with a capital if not needed and only use capitals where you would in English sentences.*
[options="header"]
|======
|Do| Don't
|Resource labels such as "general statistics" or "United States of America" or "DOI" | Resource label "germany"
|======


== Best practices for xref:courses/paper-course.adoc[Papers]

* Always assign a "research problem" to your contributions
* Make use of different contributions to organize information within the paper