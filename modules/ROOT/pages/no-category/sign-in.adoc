= Sign up & Sign in

The ORKG provides its knowledge open and free. To be able to see it, you do not have to register an account or sign in at all. Also all data access tools are ready for you to use without an ORKG account.

However, if you wish to contribute papers, comparisons or any other type of ORKG content, you need to create an account and make sure to sign in before doing so. This way all the hard work you put into adding scholarly knowledge to the ORKG can be attributed to you.

Signing up is easy and only takes a minute and you just need to provide an e-mail address.

To sign in, you need to click on the Sign In button on the top right of the ORKG main page. This will open the below window for you:

image::signin.PNG[]

If you already have an account, you can simply enter your credentials here and click on Sign in. To create an account you need to click on the corresponding button right below the sign in button. This opens the Sign up window for you:

image::signup.PNG[]

Here, you need to fill in the display name that shows up next to everything you add to the ORKG, an e-mail address to confirm your sign up and you need to think of a password. Then you need to accept our data policies and click Sign up. 

You just created an ORKG account. Welcome to the ORKG!

== What next?
* Not sure yet, why to even create an account? Check out this related question: xref:q-and-as/why-should-i-contribute-to-the-orkg.adoc[]

== Related Questions
* xref:q-and-as/how-can-i-delete-my-account.adoc[]
* xref:q-and-as/forgot-password.adoc[]
* xref:q-and-as/change-password.adoc[]