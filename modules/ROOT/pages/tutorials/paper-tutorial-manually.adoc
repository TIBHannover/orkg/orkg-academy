include::../partials/paper-tutorial-manually.adoc[]

== What next?

* Want to know more about papers in general? Check out our xref:courses/paper-course.adoc[Papers and Contributions Course].
* Want to see, what your paper looks like in its graph form? See this tutorial for the xref:tutorials/graph-view.adoc[Graph Visualization Tool].